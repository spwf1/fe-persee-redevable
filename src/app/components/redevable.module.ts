import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomeComponent} from "./home/home.component";
import {RedevableRoutingModule} from "./redevable-routing.module";
import {FePerseeCommunModule} from "fe-persee-commun";

@NgModule({
  declarations: [HomeComponent],
    imports: [CommonModule, RedevableRoutingModule, FormsModule, ReactiveFormsModule, FePerseeCommunModule],
})
export class RedevableModule {}
