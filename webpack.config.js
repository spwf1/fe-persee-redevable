const { shareAll, withModuleFederationPlugin } = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({

  name: 'fe-persee-redevable',

  exposes: {
    './Module' : './src/app/components/redevable.module.ts',
    MenuEntry : './src/app/components/redevable-menu.json'

  },

  shared: {
    ...shareAll({ singleton: true, strictVersion: true, requiredVersion: 'auto' }),
  },

});
